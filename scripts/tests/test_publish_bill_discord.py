import os
import unittest
import sys
import subprocess

try:
    # to make my life in the IDE easier
    from scripts import publish_bill_discord
except ImportError:
    import publish_bill_discord

FROM = publish_bill_discord.FROM


class TestPublishBillDiscord(unittest.TestCase):

    def test_direct_function(self):
        base_string = """
    ORDERRRR, Order!
    
    The item on the agenda:
    
    :commons: CB01-01: Fair Employee Representation Bill
    
    *A bill to establish worker representation through Inclusive Ownership Funds and employee board seats.*
    
    :libdem::labour: This bill was written by The Rt Hon Hywel ap Iorwerth 1st Earl of Caernarfon KCTL, and George Vere, on behalf of HM Government.
    <!91249124014><!91249124014>""".rstrip()
        publish_bill_response = publish_bill_discord.get_discord_bill_text("cb01-01", 0, publish_bill_discord.FROM.GOV,
                                                                           "house-of-commons").rstrip()
        self.assertEqual(base_string, publish_bill_response)

    def test_direct_function_on_behalf_of(self):
        base_string = """
    ORDERRRR, Order!
    
    The item on the agenda:
    
    :commons: CB01-01: Fair Employee Representation Bill
    
    *A bill to establish worker representation through Inclusive Ownership Funds and employee board seats.*
    
    :libdem::labour: This bill was written by The Rt Hon Hywel ap Iorwerth 1st Earl of Caernarfon KCTL, and George Vere{}.
    <!91249124014><!91249124014>""".rstrip()

        for on_behalf_of in (FROM.PRIVATE_MEMBER, FROM.GOV, FROM.OPP, FROM.NONE):
            # very lazy
            match on_behalf_of:
                case FROM.PRIVATE_MEMBER:
                    on_behalf_of_text = " as a private member's bill"
                case FROM.GOV:
                    on_behalf_of_text = ", on behalf of HM Government"
                case FROM.OPP:
                    on_behalf_of_text = ", on behalf of HM Opposition"
                case _:
                    on_behalf_of_text = ""
            new_string = base_string.format(on_behalf_of_text)

            publish_bill_response = publish_bill_discord.get_discord_bill_text("cb01-01", 0,
                                                                               on_behalf_of,
                                                                               "house-of-commons").rstrip()
            self.assertEqual(new_string, publish_bill_response)

    def test_call_function_from_cli(self):
        base_string = """
    ORDERRRR, Order!
    
    The item on the agenda:
    
    :commons: CB01-01: Fair Employee Representation Bill
    
    *A bill to establish worker representation through Inclusive Ownership Funds and employee board seats.*
    
    :libdem::labour: This bill was written by The Rt Hon Hywel ap Iorwerth 1st Earl of Caernarfon KCTL, and George Vere, on behalf of HM Government.
    <!91249124014><!91249124014>""".rstrip()

        test_env = os.environ.copy()
        test_env["COMMONS"] = "0"
        cli_response = subprocess.run(["python3", "publish_bill_discord.py", "cb01-01", "0", "GOV"],
                                      env=test_env, capture_output=True).stdout.decode().rstrip()
        self.assertEqual(base_string, cli_response)

    def test_call_function_from_discord(self):
        # stub
        pass


if __name__ == '__main__':
    unittest.main()
