import requests

def send_msg_to_webhook(url: str, msg: str):
    data = {
        "content": msg,
        "username": "parliament.uk"
    }    
    result = requests.post(url, json = data)
    try:
        result.raise_for_status()
    except requests.exceptions.HTTPError as err:
        print(err)
    else:
        print("Payload delivered successfully, code {}.".format(result.status_code))
