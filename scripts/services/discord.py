
import pathlib
import interactions
import requests

GUILD_ID = "556271746592800772"
API_KEY_FILE = "keys/discord.txt"
API_KEY = pathlib.Path(API_KEY_FILE).read_text().strip()
__GUILD = None
__BOT = None
BASE_API_URL = "https://discord.com/api/10"


def send_request(endpoint) -> dict:
    headers = {
        "Authorization": f"Bot {API_KEY}"
    }
    print(f"{BASE_API_URL}{endpoint}")
    r = requests.get(f"{BASE_API_URL}{endpoint}", headers=headers)
    print(r.json())
    return r.json()

def get_emote(emote_id: str) -> str:
    return send_request(f"/guilds/{GUILD_ID}/emojis/{emote_id}")
    



def get_client() -> interactions.Client:
    global __BOT
    if not __BOT:
        with open(API_KEY_FILE, encoding="utf-8") as api_key_file:
            __BOT = interactions.Client(
                token=api_key_file.read()
            )
            
    return __BOT

def get_guild() -> interactions.Guild:
    global __GUILD
    if not __GUILD:
        client = get_client()
        for guild in client.guilds:
            if guild.id == GUILD_ID:
                __GUILD = guild
                break
    return __GUILD