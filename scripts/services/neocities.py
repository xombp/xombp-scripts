"""
This script is responsible for generating the default files for
a bill based on the bill.md file located in the bill's directory
"""


from pathlib import Path
import requests


__author__ = "Preben Vangberg"
__copyright__ = "Copyright 2023, XO's Model British Parliament"
__credits__ = ["Preben Vangberg"]
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Preben Vangberg"
__email__ = "prvinspace@gmail.com"
__status__ = "Production"


class Neocities:
    """Represents a connection to a Neocities page"""

    def __init__(self, api_key: str = None) -> None:
        api_key_path = Path("keys/neocities.txt")
        if api_key_path.exists() and not api_key:
            with open(api_key_path, "r", encoding="utf-8") as api_key_file:
                api_key = api_key_file.read()
        if not api_key:
            raise ValueError("API key is empty")
        self.__api_key = api_key

    def __get_header_data(self) -> dict[str, str]:
        """Generate the default header data"""
        return {"Authorization": f"Bearer {self.__api_key.strip()}"}

    def file_exists(self, file: str, path: str = None) -> bool:
        """Checks if a file exists on the page"""
        for f in self.get_file_list(path):
            if f['path'] == file:
                return True
        return False

    def get_file_list(self, path="") -> list:
        """Returns a list of files present in the Neocities page. 'path' used to narrow search"""
        data = requests.get(
            "https://neocities.org/api/list",
            params={"path": path} if path else {},
            headers=self.__get_header_data(),
            timeout=10
        ).json()
        return data['files']

    def upload(self, path, file_on_disk) -> None:
        """Uploads the file to the page"""
        with open(file_on_disk, 'rb') as file:
            data = {path: file}
            message = requests.post(
                "https://neocities.org/api/upload",
                files=data,
                headers=self.__get_header_data(),
                timeout=10
            )
            if message.status_code != 200:
                print(message.json()['message'])
            else:
                print("Upload successful!")
