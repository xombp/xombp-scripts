
import gspread
from gspread.utils import ValueInputOption
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
import string
from entities.house import House

PARTY_TO_COLOUR = {
    "Labour": {
        'bg': "#d11428",
        'text': "#ffffff"
    }
}


def cache(function):
    last_cache = datetime.now() + timedelta(hours=-24)
    value = None
    def wrapper(*args, **kwargs):
        nonlocal value, last_cache
        if value is None or datetime.now() > last_cache + timedelta(hours=1):
            value = function(*args, **kwargs)
            last_cache = datetime.now()
        return value
    return wrapper

class XOMBPSpreadsheet:

    def __init__(self) -> None:
        gc = gspread.service_account(filename='keys/gsheets.json')
        self.__spreadsheet: gspread.Spreadsheet = gc.open_by_key('1IQxNJkf9ECDAOIsUUNGnZXAMseLtOsbRzqg7RIHNfcU')
        self.__database: gspread.Spreadsheet = gc.open_by_key("1tyP1IIKz0w3qcXjAuI-KiTw1YwEaFAfGVcyOC7fU1zo")

    @cache
    def get_characters(self) -> pd.DataFrame:
        worksheet: gspread.Worksheet = self.__database.worksheet("Characters")
        dataframe = pd.DataFrame(worksheet.get_all_records(head=2))
        return dataframe

    @cache
    def get_users(self) -> pd.DataFrame:
        worksheet: gspread.Worksheet = self.__database.worksheet("Users")
        dataframe = pd.DataFrame(worksheet.get_all_records(head=2))
        return dataframe

    @cache
    def get_parties(self) -> pd.DataFrame:
        worksheet: gspread.Worksheet = self.__database.worksheet("Parties")
        dataframe = pd.DataFrame(worksheet.get_all_records(head=2))
        return dataframe
    
    @cache
    def get_bills(self) -> pd.DataFrame:
        columns=[
            "Bill No.",
            "Short title",
            "Status",
            "Author",
            "House 1",
            "House 1 1st reading",
            "House 1 2nd reading",
            "House 1 Report",
            "House 1 3rd reading",
            "House 2",
            "House 2 1st reading",
            "House 2 2nd Reading",
            "House 2 Report",
            "House 2 3rd reading",
            "House 3",
            "House 3 Consideration",
            "House 4",
            "House 4 Consideration"
        ]
        
        # Fetch the database one
        worksheet: gspread.Worksheet = self.__database.worksheet("Bills")
        df = pd.DataFrame(worksheet.get_all_values()[3:], columns=columns)

        # Fetch the current one
        worksheet = self.__spreadsheet.worksheet("11th Term Bills")
        current_df = pd.DataFrame(worksheet.get_all_values()[3:], columns=columns)
        
        # Clean up so that the content is consistent
        df = cleanup_bill_dataframe(df)
        current_df = cleanup_bill_dataframe(current_df)

        # Set the bill id as index so that we can merge
        df = df.set_index('Bill No.')
        current_df = current_df.set_index('Bill No.')
        
        # This updates existing entires
        df.update(current_df)

        # This add entries that doesn't exist
        df = pd.concat([df, current_df[~current_df.index.isin(df.index)]])

        return df.reset_index()
        

    def get_character_by_id(self, char_id: str) -> dict:
        chars = self.get_characters()
        chars = chars[chars["ID"]==char_id]
        if len(chars) == 0:
            return None
        return chars.iloc[0].to_dict()

    def get_character_by_name(self, char_name: str) -> list[dict]:
        char_name = char_name.replace(".", "")
        char_name = '.*'.join(char_name.split())
        chars = self.get_characters()
        chars = chars[chars["Full-name"].str.contains(char_name, case=False, regex=True)]
        return chars.to_dict('records')

    def get_user_by_discord_id(self, user_id) -> dict:
        chars = self.get_users()
        chars = chars[chars["Discord ID"]==user_id]
        if len(chars) == 0:
            return None
        return chars.iloc[0].to_dict()

    def get_party_by_id(self, party_id: str) -> dict:
        parties = self.get_parties()
        parties = parties[parties["Party ID"]==party_id]
        if len(parties) == 0:
            return None
        return parties.iloc[0].to_dict()

    def add_bill(self, bill) -> None:
        ws = self.__spreadsheet.get_worksheet_by_id(806386324)
        main_author = bill.data['main_author'] if 'main_author' in bill.data else \
            list(bill.data['authors'].values())[0]
        author_name = main_author['name'] if bill.data['submission_type'] == "private" \
            else "HM Government" if bill.data['submission_type'] == "government" \
            else "HM Opposition"

        print(main_author)
        ws.get_all_values()
        house_order = ["C", "L"] if bill.data['id'].startswith("C") else ["L", "C"]
        ws.insert_row(
            [bill.data['id'],
                bill.data['effective_title'], 
                "2nd Reading", 
                author_name, 
                house_order[0],
                get_date_string(),
                "-", "", "",
                house_order[1],
                "", "", "","",
                house_order[0], "", house_order[1]
            ],
            index = ws.row_count + 1,
            value_input_option=ValueInputOption.user_entered,
            inherit_from_before=True
        )

        color = PARTY_TO_COLOUR.get(main_author['party'], {
            'bg': "#d9d9d9",
            'text': "#000000"
        })

        border = {
            'style': "SOLID"
        }
        houseColor = "#006547" if house_order[0] == "C" else "#b41d44"
        textFormat = {
                "fontFamily": "Roboto Condensed",
                "fontSize": 10 ,
                "foregroundColor": hex_to_rgb("#ffffff")
        }

        # Format the whole line
        ws.format(f"A{ws.row_count+1}:{ws.row_count+1}", {
            "backgroundColor": hex_to_rgb("#d0cede"),
            'textFormat': textFormat,
            'borders': {
                'top': border,
                'bottom': border,
                'left': border,
                'right': border
            }
        })

        # Format the author field
        ws.format(f"D{ws.row_count+1}", {
            "backgroundColor": hex_to_rgb(color['bg']),
            'textFormat': { **textFormat, 
                "foregroundColor": hex_to_rgb(color['text'])
            }
        })

        # Color house fields
        ws.format(f"A{ws.row_count+1}", {
            "backgroundColor": hex_to_rgb(houseColor)
        })
        ws.format(f"C{ws.row_count+1}", {
            "backgroundColor": hex_to_rgb(houseColor)
        })
        ws.format(f"G{ws.row_count+1}", {
            'textFormat': {**textFormat,  'foregroundColor': hex_to_rgb("#000000")}
        })

        # Format the readings
        ws.format(f"F{ws.row_count+1}:G{ws.row_count+1}", {
            "backgroundColor": hex_to_rgb("#b6d7a8"),
            'textFormat': { **textFormat, 
                "foregroundColor": hex_to_rgb("#000000")
            }
        })

        # Format the link
        ws.format(f"B{ws.row_count+1}", {
            "textFormat": {**textFormat,
                'foregroundColor': hex_to_rgb("#000000"),
                'underline': True,
                "link": {
                    "uri": f"https://xombp.neocities.org/legislation/{bill.data['id'].lower()}.html"
                }
            }
        })

    def add_act_of_parliament(self, bill):
        ws = self.__spreadsheet.get_worksheet_by_id(2051194293)
        main_author = bill.data['main_author'] if 'main_author' in bill.data else \
            list(bill.data['authors'].values())[0]
        author_name = main_author['name'] if bill.data['submission_type'] == "private" \
            else "HM Government" if bill.data['submission_type'] == "government" \
            else "HM Opposition"

        print(main_author)
        ws.get_all_values()
        ws.insert_row(
            [   "", # Term
                bill.data['id'],
                f"c.{bill.data['chapter']}",
                bill.data['effective_title'], 
                author_name, 
                bill.data['stages']['0']['date'],
                bill.data['enacted']
            ],
            index = ws.row_count + 1,
            value_input_option=ValueInputOption.user_entered,
            inherit_from_before=True
        )

        color = PARTY_TO_COLOUR.get(main_author['party'], {
            'bg': "#cccccc",
            'text': "#000000"
        })

        border = {
            'style': "SOLID"
        }
        textFormat = {
                "fontFamily": "Roboto Condensed",
                "fontSize": 10 ,
                "foregroundColor": hex_to_rgb("#000000")
        }

        # Format the whole line
        ws.format(f"A{ws.row_count+1}:{ws.row_count+1}", {
            "backgroundColor": hex_to_rgb("#d0cede"),
            'textFormat': textFormat,
            'borders': {
                'top': border,
                'bottom': border,
                'left': border,
                'right': border
            }
        })

        # Format the author field
        ws.format(f"E{ws.row_count+1}", {
            "backgroundColor": hex_to_rgb(color['bg']),
            'textFormat': { **textFormat, 
                "foregroundColor": hex_to_rgb(color['text'])
            }
        })

        # Format the link
        ws.format(f"D{ws.row_count+1}", {
            "backgroundColor": hex_to_rgb("#a29eb6"),
            "textFormat": {**textFormat,
                'foregroundColor': hex_to_rgb("#000000"),
                'underline': True,
                "link": {
                    "uri": f"https://xombp.neocities.org/legislation/{bill.data['id'].lower()}.html"
                }
            }
        })

    def get_current_mps(self) -> pd.DataFrame:
        worksheet: gspread.Worksheet = self.__spreadsheet.get_worksheet_by_id(2135113513)
        data = worksheet.get_values("C2:E")
        data.pop(1)
        columns = data.pop(0)
        while not data[-1][0]:
            data.pop()
        return pd.DataFrame(
            data=data,
            columns=columns
        )
    
    def get_current_peers(self) -> pd.DataFrame:
        worksheet: gspread.Worksheet = self.__spreadsheet.get_worksheet_by_id(1461303176)
        data = worksheet.get_values("C2:E")
        data.pop(1)
        columns = data.pop(0)
        while not data[-1][0]:
            data.pop()
        return pd.DataFrame(
            data=data,
            columns=columns
        )


    def add_votes(self, house: House, title: str, division: str, votes: list, speaker: str) -> None:
        ws = self.__spreadsheet.get_worksheet_by_id(house.value.votes_sheet)
        col = col_num_to_letter(ws.col_count)
        row = ws.row_count

        aye = "AYE" if house == House.Commons else "CON"
        abs = "ABS" if house == House.Commons else "PRE"
        no = "NO" if house == House.Commons else "NOT"

        # Insert the new column
        ws.insert_cols(
            [['', title, division, *votes, "", speaker,
                f'=COUNTIF({col}$4:{col}${len(votes)+5}, "{aye}")',
                f'=COUNTIF({col}$4:{col}${len(votes)+5}, "{no}")',
                f'=COUNTIF({col}$4:{col}${len(votes)+5}, "{abs}")',
                f'=COUNTIF({col}$4:{col}${len(votes)+5}, "DNV")',
                f'=IF({col}${row-6} > {col}${row-5}, "{aye}", IF({col}${row-5} > {col}${row-6}, "{no}", "N/A"))',
                f'={col}${row-6}+{col}${row-5}+{col}${row-4}',
                f'=IFERROR({col}${row-1} / ({col}${row-1} + {col}${row-3}), "N/A")'
            ]],
            col=ws.col_count+1,
            inherit_from_before=True,
            value_input_option=ValueInputOption.user_entered
        )
        # Merge cells if there is no division title
        if not division:
            ws.merge_cells(f"{col}2:{col}3", merge_type="MERGE_ALL")
        border = {
            'style': "SOLID"
        }
        ws.format(f"{col}4:{col}",{
            'borders': {
                'top': border,
                'bottom': border,
                'left': border,
                'right': border
            }
        })

        # Add the bold lines for the parties
        party_names = ws.get_values("A4:B")
        for i, row in enumerate(party_names):
            if row[0] or row[1]:
                ws.format(f"{col}{4+i}",{
                    'borders': {
                        'top': { "style": "SOLID_MEDIUM" },
                        'bottom': { "style": "SOLID" },
                        'left': { "style": "SOLID" },
                        'right': { "style": "SOLID" }
                    }
                })

__DATA = None
def get_xombp_data() -> XOMBPSpreadsheet:
    global __DATA
    if __DATA is None:
        __DATA = XOMBPSpreadsheet()
    return __DATA


def get_date_string() -> str:
    now = datetime.now()
    return now.strftime("%Y %B %d")


def hex_to_rgb(hex: str) -> dict[str, str]:
    hex = hex.lstrip("#")
    r, g, b = (
        int(hex[i:i+2], 16) / 255
        for i in [0, 2, 4]
    )
    return {"red": r, "green": g, "blue": b}


def col_num_to_letter(n,b=string.ascii_uppercase):
   d, m = divmod(n,len(b))
   return col_num_to_letter(d-1,b)+b[m] if d else b[m]


def cleanup_bill_dataframe(bill_df: pd.DataFrame) -> pd.DataFrame:
    columns_to_fill = [
        'Short title', "Status", "Author", "House 1", "House 2", "House 3", "House 4"
    ]
    bill_df[columns_to_fill] = bill_df[columns_to_fill].replace("", np.NaN).fillna(method='ffill')
    bill_df = bill_df[~bill_df['Bill No.'].str.contains("^Term", regex=True)].copy()
    return bill_df.replace("^(N/A|n/a|-)$", "", regex=True)
