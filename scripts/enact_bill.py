#!/usr/bin/env python3
"""
This script is responsible for generating the default files for
a bill based on the bill.md file located in the bill's directory
"""

import argparse
from pathlib import Path
import json
import datetime

from services import discord
import services.spreadsheet as spreadsheet
import services.webhooks as webhooks
from utils.template import get_jinja2_template
from publish_bill import publish_bill, publish_first_reading
from entities.house import House
from entities.bill import load_bill
from services.webhooks import send_msg_to_webhook

__author__ = "Preben Vangberg"
__copyright__ = "Copyright 2023, XO's Model British Parliament"
__credits__ = ["Preben Vangberg"]
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Preben Vangberg"
__email__ = "prvinspace@gmail.com"
__status__ = "Production"


def main():
    """The main function of the script"""
    args = parse_args()

    bill_id = args.bill_id.lower()
    if args.term:
        bill_term = args.term
    else:
        bill_term = int(bill_id.split('-')[0][2:])
    if not bill_term:
        raise RuntimeError("No bill term provided")

    bill = load_bill(bill_id, bill_term, raw=True)
    bill.data['enacted'] = args.date.strftime("%Y-%m-%d")
    bill.data['chapter'] = args.chapter

    stage_id = max(map(int, bill.data['stages'].keys())) + 1
    bill.data['stages'][stage_id] = {
        "date": args.date.strftime('%Y-%m-%d'),
        "type": "royal_assent",
        "house": "royals"
    }

    with open("test.json", "w") as output:
        json.dump(bill.data, output, indent=2)

    bill.save()
    bill = load_bill(bill_id, bill_term)
    if args.publish:
        data = spreadsheet.get_xombp_data()
        data.add_act_of_parliament(load_bill(bill_id, bill_term))
        publish_bill(bill_id)
        publish_act_to_discord(bill)


def publish_act_to_discord(bill):
    xombp_data = spreadsheet.get_xombp_data()
    template = get_jinja2_template("enactment.msg.j2")
    authors = ", ".join(map(lambda x: x['name'], bill.data["authors"].values()))
    parties = list(map(lambda x: x['party'], bill.data["authors"].values()))
    party_emotes = {
        xombp_data.get_party_by_id(party_id)['Emote ID']
        for party_id in parties
    }
    submission_type = {
        "private": "as a private member's bill",
        "government": "on behalf of HM Government",
        "opposition": "on behalf of HM Most Loyal Opposition"
    }[bill.data['submission_type']]
    output = template.render(
        house_emote = "<:royals:560234592678838273>",
        bill = bill.data,
        authors=authors,
        submission_type=submission_type,
        party_emote=''.join(party_emotes)
    )
    send_msg_to_webhook(
        "https://discord.com/api/webhooks/1099048620159934556/vHa2XT52wRrWLjhFAZcGdRwOQr_M7aXQH76Dv-nZ0RcGN2-6eE02aVe1vrdTLMjse6sS",
        output
    )
    


def parse_args() -> argparse.Namespace:
    """Parses the arguments from the command line"""
    parser = argparse.ArgumentParser(
        "Publishes a bill to the required places"
    )
    parser.add_argument("bill_id")
    parser.add_argument("--term", nargs="?")
    parser.add_argument('--date', type=lambda s: datetime.datetime.strptime(s, '%Y-%m-%d'), default=datetime.datetime.now())
    parser.add_argument("--chapter", required=True, type=int)
    parser.add_argument("--publish", action="store_true",
                    help="Publishes the bill as well")
    return parser.parse_args()


import re
def chapter_number(arg_value, pat=re.compile(r"^c\.[1-9]+$")):
    if not pat.match(arg_value):
        raise argparse.ArgumentTypeError
    return arg_value

if __name__ == "__main__":
    main()
