#!/usr/bin/env python3
"""
This is the main entry-point for the Discord bot. A lot of the
commands are separated out into different scripts in the discord
folder
"""

import traceback
import interactions
#from interactions import Client, Message, ReactionRequest, autodefer

from entities.bill import get_bill_path
import publish_bill_discord
from publish_bill_discord import FROM

# Import of commands

from discord.character import print_character
from discord.call_division import call_division
from discord.legislation import legislation
from discord.tally import tally

__author__ = "Preben Vangberg"
__copyright__ = "Copyright 2023, XO's Model British Parliament"
__credits__ = ["Preben Vangberg"]
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Preben Vangberg"
__email__ = "prvinspace@gmail.com"
__status__ = "Production"

API_KEY_FILE = "keys/discord.txt"

with open(API_KEY_FILE, encoding="utf-8") as api_key_file:
    bot = interactions.Client(
        token=api_key_file.read()
    )

async def run_command(ctx: interactions.SlashContext, function, *args, **kwargs):
    await ctx.defer()
    print(len(args), args, kwargs)
    try:
        embed = await function(ctx, *args, **kwargs)
    except:
        print(traceback.format_exc())
        embed = interactions.Embed(
            title="An error occured",
            description="An uncaught exception uccurred and I don't know why",
            color=0xFF0000
        )

    if ctx.responded:
        return

    if type(embed) == str:
        await ctx.send(embed)
    else:
        await ctx.send(embeds=[embed])

@interactions.slash_command(
    name="character",
    description="Tries to fetch character using the name, id, or callers Discord ID.",
)
@interactions.slash_option(
    name="char_name",
    description="The ID of the legislation",
    opt_type=interactions.OptionType.STRING,
)
@interactions.slash_option(
    name="char_id",
    description="The ID of the legislation",
    opt_type=interactions.OptionType.INTEGER
)   
async def __print_character(ctx, **kwargs):
    await run_command(ctx, print_character, **kwargs)

@interactions.slash_command(
    name="call-division",
    description="Calls a division for the given bill / motion"
)
@interactions.slash_option(
    name="leg_id",
    description="The ID of the legislation",
    opt_type=interactions.OptionType.STRING,
    required=True
)
@interactions.slash_option(
    name="division_type",
    description="The type of the division",
    opt_type=interactions.OptionType.STRING,
    required=True
)
@interactions.slash_option(
    name="house",
    description="The house of the division",
    opt_type=interactions.OptionType.CHANNEL,
    required=True
)   
async def __call_division(ctx: interactions.SlashContext,
                          leg_id: str,
                          division_type: str,
                          house: interactions.GuildChannel):
    await call_division(ctx, leg_id, division_type, house)


@interactions.slash_command(
    name="legislation",
    description="Searches for a specific piece of legislation"
)
@interactions.slash_option(
    name="leg_id",
    description="The ID of the legislation",
    opt_type=interactions.OptionType.STRING,
    required=False
)
@interactions.slash_option(
    name="leg_title",
    description="The title of the legislation",
    opt_type=interactions.OptionType.STRING,
    required=False
)
async def __legislation(ctx: interactions.SlashContext, leg_id: str = None, leg_title: str = None):
    await legislation(ctx, leg_id, leg_title)


@interactions.slash_command(
    name="publish-bill",
    description="Publishes a bill in either the Commons or the Lords"
)
@interactions.slash_option(
    name="leg_id",
    description="The ID of the legislation",
    opt_type=interactions.OptionType.STRING,
    required=True
)
@interactions.slash_option(
    name="house",
    description="The house of the division",
    opt_type=interactions.OptionType.CHANNEL,
    required=True
)
@interactions.slash_option(
    name="by",
    description="The sponsor of the legislation (NONE, PRIVATE, GOV, or OPP)",
    opt_type=interactions.OptionType.CHANNEL,
    required=True
)
# not tested, and also not async
async def __publish_bill(ctx: interactions.SlashContext, leg_id: str, house: interactions.GuildChannel, by: str):
    match by.upper():
        case "NONE":
            on_behalf_of = FROM.NONE
        case "PRIVATE":
            on_behalf_of = FROM.PRIVATE_MEMBER
        case "GOV":
            on_behalf_of = FROM.GOV
        case "OPP":
            on_behalf_of = FROM.OPP
        case _:
            on_behalf_of = int(by)

    bill_text = publish_bill_discord.get_discord_bill_text(leg_id, int(house.id), on_behalf_of, house.name)
    house.send(bill_text)


@interactions.slash_command(
    name="tally-manual",
    description="Tallies the votes for a division and ads it to the spreadsheet."
)
@interactions.slash_option(
    name="msg",
    description="The message to extract the votes from",
    opt_type=interactions.OptionType.STRING,
    required=True
)
@interactions.slash_option(
    name="channel",
    description="The channel the message is in",
    opt_type=interactions.OptionType.CHANNEL,
    required=True
)
@interactions.slash_option(
    name="id",
    description="The ID of the bill. Only required if it can't be determined from the message",
    opt_type=interactions.OptionType.STRING,
    required=False
)
@interactions.slash_option(
    name="division",
    description="The name of the division. Only required if it can't be determined from the message",
    opt_type=interactions.OptionType.STRING,
    required=False
)
@interactions.slash_option(
    name="title",
    description="The title of the bill. Only required if it can't be determined from the message",
    opt_type=interactions.OptionType.STRING,
    required=False
)
async def __tally_manual(ctx: interactions.SlashContext, **kwargs):
    await run_command(ctx, tally, **kwargs)


@interactions.message_context_menu(name="Tally")
async def __tally_context(ctx: interactions.ContextMenuContext):
    message: interactions.Message = ctx.target
    await run_command(ctx, tally, channel=ctx.channel, msg=message.id)



# Starts the bot
bot.start()
