#!/usr/bin/env python3


import argparse
from pathlib import Path

from entities.character import load_character, update_character_list, ALL_CHARACTERS_PATH
from entities.party import load_party
from services.neocities import Neocities
from utils.template import get_jinja2_template


def publish_character(id: str):
    character = load_character(int(id))

    parties = {}
    if 'parties' not in character:
        character['parties'] = {}
    for party in character['parties'].values():
        if party['party'] not in parties:
            parties[party['party']] = load_party(party['party'])
    print(parties)
    template = get_jinja2_template('character.html.j2')
    print(character)
    html = template.render(
        char=character,
        parties=parties
    )

    temp_path = Path("tmp/char_{id}.html")
    temp_path.parent.mkdir(parents=True, exist_ok=True)

    with open(temp_path, "w", encoding="utf-8") as outfile:
        outfile.write(html)

    nc = Neocities()
    nc.upload(f"characters/id/{id}.html", temp_path)
    temp_path.unlink()


def publish_character_list():
    update_character_list()
    nc = Neocities()
    nc.upload("characters/characters.json", ALL_CHARACTERS_PATH)


def main():
    parser = argparse.ArgumentParser(
        "Publishes a character to the required places"
    )
    parser.add_argument("character")
    args = parser.parse_args()
    publish_character(args.character)
    # update_character_list()


if __name__ == "__main__":
    main()
