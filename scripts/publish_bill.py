#!/usr/bin/env python3
"""
This script is responsible for publishing a bill to the
Neocities page
"""

import argparse
from datetime import datetime
import dateutil.parser as dparse
from services.neocities import Neocities
from services.spreadsheet import get_xombp_data
from entities.house import House
from entities.bill import load_bill
from utils.template import get_jinja2_template
from services.webhooks import send_msg_to_webhook

__author__ = "Preben Vangberg"
__copyright__ = "Copyright 2023, XO's Model British Parliament"
__credits__ = ["Preben Vangberg"]
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Preben Vangberg"
__email__ = "prvinspace@gmail.com"
__status__ = "Production"


def main():
    """The main function of the program"""
    args = parse_args()
    data = get_xombp_data()
    bill = load_bill(args.bill)
    if 'versions' not in bill.data:
        raise Exception("No version available")
    publish_bill(args.bill)

    if args.spreadsheet:
        data.add_bill(bill)
        publish_bill(args.bill)

    if args.discord:
        publish_first_reading(args.bill)


def publish_bill(bill_id):
    """Publishes the bill with the given ID to the correct locations"""
    bill = load_bill(bill_id.lower())
    print(bill)
    template = get_jinja2_template('bill.html.j2')
    versions = {
        version['id']: bill.get_version_html(version['id']) for version_id, version in bill.data['versions'].items()
    }

    bill.data['introduced'] = convert_date(
        bill.data['introduced']
    )

    if 'enacted' in bill.data:
        bill.data['enacted'] = convert_date(
            bill.data['enacted']
        )
    

    data = get_xombp_data()
    for author_id, author in bill.data['authors'].items():
        author_data = data.get_character_by_id(int(author_id))
        author['fullname'] = author_data['Full-name'].replace(
            author_data['Name'], f"<b>{author_data['Name']}</b>"
        )


    html = template.render(
        bill=bill.data,
        versions=versions
    )

    with open("temp.html", "w", encoding="utf-8") as outfile:
        outfile.write(html)

    nc = Neocities()
    nc.upload(f"legislation/{bill_id.lower()}.html", "temp.html")
    import os
    os.unlink("temp.html")

def convert_date(date_str):
    return dparse.parse(date_str).strftime(
        "%Y %B %d"
    )


def publish_first_reading(bill_id):
    xombp_data = get_xombp_data()
    bill = load_bill(bill_id.lower())
    house = House.Lords.value if bill.data['id'].startswith("L") else House.Commons.value
    template = get_jinja2_template("first_reading.msg.j2")
    authors = ", ".join(map(lambda x: x['name'], bill.data["authors"].values()))
    parties = list(map(lambda x: x['party'], bill.data["authors"].values()))
    party_emotes = {
        xombp_data.get_party_by_id(party_id)['Emote ID']
        for party_id in parties
    }
    submission_type = {
        "private": "as a private member's bill",
        "government": "on behalf of HM Government",
        "opposition": "on behalf of HM Most Loyal Opposition"
    }[bill.data['submission_type']]
    output = template.render(
        house_emote = house.emote_id,
        bill = bill.data,
        authors=authors,
        submission_type=submission_type,
        party_emote=''.join(party_emotes)
    )
    send_msg_to_webhook(
        "https://discord.com/api/webhooks/1092911590656974999/20lwfs5-waDnfHI2vjfg0v0_Tfq5vMuDtFsN0qfc6O4zMZpQwkexMcPxMDdNJI1vq3dX",
        output
    )
    


def parse_args() -> argparse.Namespace:
    """Parses the arguments from the command line"""
    parser = argparse.ArgumentParser(
        "Publishes a bill to the required places"
    )
    parser.add_argument("bill")
    #parser.add_argument("term", default=None)
    parser.add_argument("--discord", action="store_true",
                        help="Publishes the bill as well")
    parser.add_argument("--spreadsheet", action="store_true",
                        help="Publishes the bill as well")
    return parser.parse_args()


if __name__ == "__main__":
    main()
