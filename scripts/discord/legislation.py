"""
This file contains the functions for the legislation command
"""

import interactions
from interactions import Embed
from entities.bill import get_bill_path, load_bill


__author__ = "Preben Vangberg"
__copyright__ = "Copyright 2023, XO's Model British Parliament"
__credits__ = ["Preben Vangberg"]
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Preben Vangberg"
__email__ = "prvinspace@gmail.com"
__status__ = "Production"


async def legislation(ctx: interactions.ComponentContext, leg_id: str = None, leg_title: str = None):
    if not leg_id and not leg_title:
        await ctx.send("You need to provide either an ID or search term")
        return

    if not leg_id:
        await ctx.send("Search by term not implemented")
        return

    path = get_bill_path(leg_id)
    if not path.exists():
        await ctx.send(f"Legislation with ID {leg_id!r} not found")

    leg = load_bill(leg_id)

    embed = Embed(
        title=leg.data['effective_title'],
        description=leg.data['effective_long_title'],
        color=0x5F2DB4
    )

    embed.add_field(
        "First reading",
        leg.data['stages']['0']['date'],
        inline=True
    )
    last_stage = str(max(map(int, leg.data['stages'].keys())))
    embed.add_field(
        "Last stage",
        leg.data['stages'][last_stage]['type'].replace("_", " ").capitalize(),
        inline=True
    )

    embed.add_field(
        "Last stage (date)",
        leg.data['stages'][last_stage]['date'],
        inline=True
    )

    embed.add_field(
        "Author(s)",
        "\n".join(sorted(
            map(lambda author: author['name'], leg.data['authors'].values())
        )),
        inline=False
    )

    await ctx.send(embeds=[embed])
