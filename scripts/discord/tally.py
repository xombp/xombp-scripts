
import interactions
import re
import services.spreadsheet
from entities.bill import load_bill
from entities.house import House
import urllib.parse
async def tally(ctx: interactions.SlashContext, msg: int, channel: interactions.GuildText, id: str = None, division: str = None, title: str = None):

    # Check if user is a deputy speaker
    if not 556489506765668362 in ctx.author.roles:
        await ctx.send("User not authorised to perform that actions.", delete_after=5)
        return

    # Defer because this takes a bit to process
    if not ctx.deferred:
        await ctx.defer()
    
    # Find the message
    is_lords = "lords" in channel.name
    message = channel.get_message(msg)
    if message is None:
        await ctx.send("Message was not found", delete_after=5)
        return

    # Determine the Bill / Motion ID
    if id is None:
        id = re.search(r"[CL][BM][0-9]{1,2}-[0-9]{1,3}", message.content, re.IGNORECASE)
        if not id:
            await ctx.send("Unable to determine the id of the bill. Please use the id parameter to specify it manually", delete_after=5)
            return
        id = id.group().strip()

    # Determine the title of the division
    if division is None:
        if id[1] == "M":
            division = ""
        if re.search(r"(2nd|second reading)", message.content, re.IGNORECASE):
            division = "2nd"
        elif re.search(r"(3rd|third reading)", message.content, re.IGNORECASE):
            division = "3rd"
        elif (m := re.search(r"(?<=Amendment )[0-9A-Z]+(?=[* ])", message.content, re.IGNORECASE)):
            division = "Amdt " + m.group()
        elif re.search(r"(motion to agree to all)", message.content, re.IGNORECASE):
            division = "CoA"
        else:
            await ctx.send("Division title (reading) not found. Please use the division parameter to specify it manually.", delete_after=5)
            return

    # Attempt to fetch the bill name
    data = services.spreadsheet.get_xombp_data()
    if not title:
        bill = load_bill(id.upper())
        title = bill.data['effective_title']

    emotes = {
        "✅": "AYE",
        "abstain": "ABS"
    }

    # Fetch the votes from the reactions of the message
    votes = {}
    for reaction in message.reactions:
        #message.fetch_reaction(reaction.users)
        #print(reaction.emoji.)
        #emote = f"{reaction.emoji.require_colons}:{reaction.emoji.id}" if reaction.emoji.id is not None else urllib.parse.quote(reaction.emoji.name)
        #users = await  ctx.client _client.get_reactions_of_emoji(message.channel_id, message.id, emote)
        for user in await reaction.users().flatten():
            votes[str(user.id)] = emotes.get(reaction.emoji.name, reaction.emoji.name.upper())

    print(votes)

    # Fetch the member and add the votes
    print(is_lords)
    members = data.get_current_peers() if is_lords else data.get_current_mps()
    print(members)
    members.rename(columns = {'MP':'Name', 'Peer':'Name'}, inplace = True)
    members['votes'] = members.apply(
        lambda row: "" if not row.Name else "N/A" if not row.ID else votes.pop(row.ID, "DNV"),
        axis=1
    )

    if len(votes):
        ctx.send(f"The following votes were not counted: {votes}", delete_after=5)
    print(members)

    # Add the speakers vote
    members.at[0, 'votes'] = "N/A"
    vote_counts = members.groupby(['votes']).size().to_dict()
    if vote_counts.get("AYE", 0) == vote_counts.get("NO", 0):
        if division == "2nd":
            members.at[0, 'votes'] = "AYE"
        else:
            members.at[0, 'votes'] = "NO"

    # Convert the fields if the division is in the HoL
    if is_lords:
        members['votes'].replace("AYE", "CON", inplace=True),
        members['votes'].replace("ABS", "PRE", inplace=True)
        members['votes'].replace("NO", "NOT", inplace=True)
    
    # Add data to the spreadsheet
    data.add_votes(
        House.Lords if is_lords else House.Commons,
        id,
        division,
        members['votes'].tolist(),
        ctx.author.username
    )

    await message.add_reaction("💾")

    aye = f"The contents," if is_lords else "The ayes to the right,"
    no = f"The not contents," if is_lords else "The noes to the left,"
    abs = f"The presents," if is_lords else "Abstentions,"

    resp = [ f"**{id.upper()}: {title}**" ]
    if vote_counts.get('AYE', 0) > 0:
        resp.append(f"{aye} {vote_counts.get('AYE')}!")
    if vote_counts.get('NO', 0) > 0:
        resp.append(f"{no} {vote_counts.get('NO')}!")
    if vote_counts.get('ABS', 0) > 0:
        resp.append(f"{abs} {vote_counts.get('ABS')}!")
    if vote_counts.get('DNV', 0) > 0:
        resp.append(f"Did not vote, {vote_counts.get('DNV')}!")

    await ctx.send("\n".join(resp), delete_after=5)
