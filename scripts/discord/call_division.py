"""
This file contains the functions for the call_division command
"""


from pathlib import Path
import re

import interactions

from entities.bill import get_bill_path, load_bill
from utils.template import get_jinja2_template


__author__ = "Preben Vangberg"
__copyright__ = "Copyright 2023, XO's Model British Parliament"
__credits__ = ["Preben Vangberg"]
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Preben Vangberg"
__email__ = "prvinspace@gmail.com"
__status__ = "Production"


HOUSES = {
    "556274285778305024": {
        "type": "commons",
        "role": "556273881489342465",
        "debate_channel": 556274201363873812,
        'what_to_clear': "lobbies"
    },
    "556274673524932638": {
        "type": "lords",
        "role": "556273902247215124",
        "debate_channel": 556274540699975701,
        'what_to_clear': "bar"
    }

}


async def call_division(ctx: interactions.SlashContext,
                        leg_id: str,
                        division_type: str,
                        house: interactions.GuildChannel):

    await ctx.defer()

    # Ensure that the bill exist
    print(f"{leg_id!r}")
    print(get_bill_path(leg_id))
    leg = load_bill(leg_id)
    if not leg:
        await ctx.send("Legislation with that ID couldn't be found")
        return
    division_type = division_type.lower()

    # Ensure that the division type is valid
    type_verification = re.compile("(2|3|coa|a[1-9]+([a-z]+)?)", re.IGNORECASE)
    if not type_verification.fullmatch(division_type):
        await ctx.send("Invalid division type. Please specify either reading (2 or 3) or amendment a<ID>")
        return

    # Ensure that the chat provided is in the list of division channels
    if str(house.id) not in HOUSES:
        await ctx.send(f"Channel {house.mention} is not in the list of division channels")
        return
        
    #leg = load_bill(leg_id)
    template = get_jinja2_template("division.msg.j2")
    role = ctx.guild.get_role(
        interactions.Snowflake(HOUSES[str(house.id)]['role'])
    )
    debate_channel = list(filter(lambda x: x.id == HOUSES[str(house.id)]['debate_channel'], ctx.guild.channels))[0]
    msg = template.render(
        role=role.mention,
        what_to_clear=HOUSES[str(house.id)]['what_to_clear'],
        leg_id=leg_id.upper(),
        leg_title=leg.data['effective_title'],
        division_title=get_division_title(division_type, HOUSES[str(house.id)]),
        leg_type="bill" if leg_id[1].upper() == "B" else "motion",
        house_chat=debate_channel.mention
    )
    print(msg)
    await house.send(msg)
    await ctx.send("Division published")


def get_division_title(division_type: str, house: dict) -> str:
    if division_type == "2":
        return "**Second Reading**"
    if division_type == "3":
        return "**Third Reading**"
    if division_type.upper().startswith("A"):
        return f"**Amendment {division_type[1:].upper()}**"
    if division_type == "coa" and house['type'] == "lords":
        return "the **Motion to Agree to all Commons Amendments**"
    if division_type == "coa" and house['type'] == "commons":
        return "the **Motion to Agree to all Lords Amendments**"
    raise NotImplementedError(f"No division text for {division_type}")
