
import interactions

from entities.bill import check_if_bill_is_valid
from entities.character import get_character_party
from entities.house import House
from entities.party import load_party
from utils.template import get_jinja2_template


async def handle_post_first_reading_cmd(ctx: interactions.CommandContext, bill_id: str, channel: interactions.Channel):
    """Handles the sanitizing the input from the /post_first_reading command before passing it on to the
    actual function"""


    emote: interactions.Emoji = await ctx.guild.get_emoji("")
    emote.
    if not check_if_bill_is_valid(bill_id):
        await ctx.send(f"Unable to locate the legislation with id {bill_id!r}")
        return

    if not any(map(lambda h: h.first_reading_channel == channel.id, House)):
        await ctx.send(
            f"The channel {channel.name!r} is not listed as a valid channel for first readings")
        return

    post_first_reading(bill_id, channel)
    await ctx.send(f"Successfully published the first reading of {bill_id}")


async def post_first_reading(bill: dict[str, str], channel: interactions.Channel):

    # Find parties for all authors
    parties: set[str] = set()
    for author in bill['authors']:
        parties.add(get_character_party(author))
        
    
    party_emotes = [
        load_party(party)['emote']
        for party in parties
    ]

    template = get_jinja2_template("first_reading.msg.j2")
    template.render(
        bill=bill,
        
    )
