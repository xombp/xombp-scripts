
import interactions
import services.spreadsheet

#interactions.Context

def print_character(ctx: interactions.SlashContext, char_name: str = None, char_id: int = None) -> interactions.Embed:
    data = services.spreadsheet.get_xombp_data()
    if char_id:
        char = data.get_character_by_id(char_id)
        if char is None:
            raise NotImplementedError()
    elif char_name:
        chars = data.get_character_by_name(char_name)
        if len(chars) == 0:
            raise NotImplementedError()
        if len(chars) > 1:
            raise NotImplementedError()
        char = chars[0]
    else:
        user = data.get_user_by_discord_id(str(ctx.author.id))
        if not user or not user['Active character']:
            raise NotImplementedError()
        char = data.get_character_by_id(user['Active character'])
    
    embed = interactions.Embed(
        title=char["Name"],
        description=char["Full-name"],
        color=0x5F2DB4
    )

    embed.add_field("ID", char["ID"], inline=True)
    embed.add_field("Player honours", char["Player honours"], inline=True)
    embed.add_field("Character honours", char["Honours"], inline=True)
    embed.add_field("Privy councillor", char["PC?"], inline=True)
    embed.add_field("MP", char["MP?"], inline=True)
    embed.add_field("Active", char["Active"], inline=True)
    embed.add_field("Party", char["Party"], inline=True)
    if char['Facesteal URL']:
        embed.set_thumbnail(char["Facesteal URL"])

    if char["Party ID"] != "not_active":
        party = data.get_party_by_id(char["Party ID"])
        if 'Colour' in party and party['Colour']:
            embed.color = int(party["Colour"][1:], 16)

    return embed