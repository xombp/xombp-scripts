
import asyncio
from dataclasses import dataclass, field
import interactions
import pathlib
import sys
import json

API_KEY_FILE = "keys/discord.txt"

with open(API_KEY_FILE, encoding="utf-8") as api_key_file:
    bot = interactions.Client(
        token=api_key_file.read()
    )

DATA_FOLDER = pathlib.Path("data/engagement/")
DATA_FOLDER.mkdir(exist_ok=True, parents=True)


@dataclass
class ChannelData:
    channel_name: str
    first_message_id: str = None
    last_message_id: str = None
    time_series: dict[str, int] = field(default_factory=dict)


async def scrapping_code() -> None:
    await bot.wait_until_ready()
    guild = await bot.fetch_guild("556271746592800772")
    print(guild.name)
    for channel in guild.channels:
        if channel.type == interactions.ChannelType.GUILD_TEXT:
            print(channel)
            await get_messages_for_channel(channel)
    await bot.stop()


async def get_messages_for_channel(channel: interactions.GuildText) -> None:
    path = DATA_FOLDER / "channels" / f"{channel.id}.json"
    path.parent.mkdir(exist_ok=True, parents=True)
    print(path)
    if path.exists():
        data = ChannelData(**json.loads(path.read_text()))
        if data.first_message_id is None:
            print("Fixing missing first message")
            data.first_message_id = data.last_message_id
            messages = await channel.fetch_messages(limit=1)
            data.last_message_id = messages[0].id
            path.write_text(json.dumps({
                "first_message_id": data.first_message_id,
                "last_message_id": data.last_message_id,
                "time_series": data.time_series
            }, indent=2))
    else:
        data = ChannelData()

    # Get messages before
    seen_ids = set()
    while True:
        # Fetch messages
        messages = await channel.fetch_messages(before=data.first_message_id, limit=100)
        if len(messages) > 0:
            data.first_message_id = messages[-1].id
            if data.last_message_id is None:
                data.last_message_id = messages[0].id

        # If there are not historical messages, fetch new messages
        elif len(messages) == 0 and not data.last_message_id is None:
            messages = await channel.fetch_messages(after=data.last_message_id, limit=100)
            if len(messages):
                data.last_message_id = messages[0].id

        # If we still got no new messages, just yeet
        if len(messages) == 0:
            return

        # Sanity check to ensure we don't have duplicates
        ids = set([message.id for message in messages])
        assert len(seen_ids.intersection(ids)) == 0
        seen_ids.update(ids)

        # Sanity check to ensure that the first message is newer than the last one
        assert messages[0].timestamp >= messages[-1].timestamp

        # Print the date for debugging purposes
        print(
            f"{messages[0].timestamp.date()} - {messages[-1].timestamp.date()} {len(messages)} msgs")

        # Count messages
        for message in messages:
            date = str(message.timestamp.date())
            data.time_series[date] = \
                data.time_series.get(date, 0) + 1
            add_message_to_user(message.author, date)

        # Save the data
        path.write_text(json.dumps({
            "channel_name": channel.name,
            "first_message_id": data.first_message_id,
            "last_message_id": data.last_message_id,
            "time_series": data.time_series
        }, indent=2))


def add_message_to_user(user: interactions.User, date: str) -> None:
    path = DATA_FOLDER / "users" / f"{user.id}.json"
    path.parent.mkdir(exist_ok=True, parents=True)
    if path.exists():
        data = json.loads(path.read_text())
    else:
        data = {}

    data['id'] = user.id
    data['is_bot'] = user.bot
    data['username'] = user.global_name
    data['nickname'] = user.display_name

    if 'time_series' not in data:
        data['time_series'] = {}
    
    data['time_series'][date] = data['time_series'].get(date, 0) + 1

    path.write_text(json.dumps(data, indent=2))


print("hello")
loop = asyncio.get_event_loop()
asyncio.ensure_future(bot.astart())
asyncio.ensure_future(scrapping_code())
loop.run_forever()
loop.close()
