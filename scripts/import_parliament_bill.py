
import argparse
import bs4

def main() -> None:
    args = get_args()
    page = bs4.BeautifulSoup(args.html_file.read(), "lxml")
    short_title = get_short_title(page)
    long_title = get_long_title(page)
    print("Short title:", short_title)
    print("Long title: ", long_title)
    extract_sections(page)


def get_short_title(page: bs4.BeautifulSoup) -> str:
    short_title = page.find("title").text
    short_title = short_title.replace("[HL]", "").strip()
    short_title = short_title.removesuffix(" Bill")
    return short_title.strip()

def get_long_title(page: bs4.BeautifulSoup) -> str:
    long_title_div = page.find(class_="longTitle")
    long_title: StopIteration = long_title_div.find_all("p")[-1].text
    long_title = long_title[0].lower() + long_title[1:]
    long_title = long_title.removesuffix(".")
    return long_title.strip()

def extract_sections(page: bs4.BeautifulSoup, level=1):
    body = page.find(class_="body")
    for section in body.find_all("section", recursive=False):
        heading = section.find("h2")
        if heading:
            print(f"## {heading.find(class_='num').text.strip()} {heading.find(class_='heading').text.strip()}")
        for child in section.children:
            print(child)
            print(child.get('class'))
            print(child.text)

        content = extract_subsections(section)

def extract_subsections(section: bs4.Tag) -> list[str]:
    ...

def extract_nested(content: bs4.Tag) -> None:
    ...

def get_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser("Converts a bill from Parliaments website to a XOMBP bill")
    parser.add_argument("html_file", type=argparse.FileType("r"))
    return parser.parse_args()

if __name__ == "__main__":
    main()