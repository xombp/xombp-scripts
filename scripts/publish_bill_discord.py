#!/usr/bin/env python3
"""
This script is responsible for publishing a bill to discord
"""
import argparse
import types

from entities.bill import load_bill
from entities.character import load_character
import os

__license__ = "MIT"

COMMONS = os.environ.get("COMMONS")
LORDS = os.environ.get("LORDS")

FROM = types.SimpleNamespace()

FROM.NONE = 0
FROM.PRIVATE_MEMBER = 1
FROM.GOV = 2
FROM.OPP = 3


def main():
    """The main function of the program"""
    args = parse_args()
    match args.on_behalf_of.upper():
        case "NONE":
            on_behalf_of = FROM.NONE
        case "PRIVATE":
            on_behalf_of = FROM.PRIVATE_MEMBER
        case "GOV":
            on_behalf_of = FROM.GOV
        case "OPP":
            on_behalf_of = FROM.OPP
        case _:
            on_behalf_of = int(args.on_behalf_of)

    print(get_discord_bill_text(args.bill, args.channel, on_behalf_of))


def get_discord_bill_text(bill_id: str, bill_channel_id: int, on_behalf_of: int = FROM.NONE,
                          bill_channel_name: str = None) -> str:
    bill_data = load_bill(bill_id.lower()).data

    house_emoji_string = "commons" if (bill_channel_id == COMMONS or bill_channel_name == "house-of-commons") \
        else "lords" if (bill_channel_id == LORDS or bill_channel_name == "house-of-lords") \
        else bill_channel_name if bill_channel_name is not None else bill_channel_id

    authors_emoji_string = ""
    authors_text = ""
    authors_ping = ""
    for author in bill_data["authors"]:
        authors_emoji_string += f':{bill_data["authors"][author]["party"]}:'

        author_character_data = load_character(author)
        authors_text += f'{author_character_data.get("prefix", "")} ' \
                        f'{author_character_data["name"]} ' \
                        f'{author_character_data.get("suffix", "")} ' \
                        f'{author_character_data.get("honours", "")}, and'

        authors_ping += f'<!{author_character_data["player"]}>'

    authors_text = authors_text.replace("and", "", authors_text.count("and") - 2).rstrip(", and").strip()

    match on_behalf_of:
        case FROM.PRIVATE_MEMBER:
            on_behalf_of_text = " as a private member's bill"
        case FROM.GOV:
            on_behalf_of_text = ", on behalf of HM Government"
        case FROM.OPP:
            on_behalf_of_text = ", on behalf of HM Opposition"
        case _:
            # should this raise error instead of failing silently?
            on_behalf_of_text = ""

    bill_text = f"""
    ORDERRRR, Order!
    
    The item on the agenda:
    
    :{house_emoji_string}: {bill_data["id"]}: {bill_data["effective_title"]}
    
    *{bill_data["effective_long_title"]}.*
    
    {authors_emoji_string} This bill was written by {authors_text}{on_behalf_of_text}.
    {authors_ping}
    """

    return bill_text


def parse_args() -> argparse.Namespace:
    """Parses the arguments from the command line"""
    parser = argparse.ArgumentParser(
        "Publishes bills to discord"
    )
    parser.add_argument("bill")
    parser.add_argument("channel")
    parser.add_argument("on_behalf_of")
    return parser.parse_args()


if __name__ == "__main__":
    main()
