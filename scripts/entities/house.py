

from enum import Enum
from dataclasses import dataclass


@dataclass
class HouseData:
    full_name: str
    role_id: str
    emote_id: str
    first_readning_channel: str
    debate_channel: str
    votes_sheet: int


class House(Enum):
    Commons = HouseData(
        "House of Commons",
        "556273881489342465",
        "<:commons:560234593123434512>",
        "1068949867587702854",
        "",
        2135113513
    )
    Lords = HouseData(
        "House of Lords",
        "556273902247215124",
        "<:lords:560234593618231297>",
        "1068949824398966835",
        "",
        1461303176
    )
