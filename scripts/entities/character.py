"""
This file contains functions and classes related to
characters
"""


from pathlib import Path
import json
import re
from typing import Any
import dateutil.parser
from services.spreadsheet import get_xombp_data

__author__ = "Preben Vangberg"
__copyright__ = "Copyright 2023, XO's Model British Parliament"
__credits__ = ["Preben Vangberg"]
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Preben Vangberg"
__email__ = "prvinspace@gmail.com"
__status__ = "Production"


ALL_CHARACTERS_PATH = Path("cache/characters.json")


def load_character(char_id: str) -> dict:
    """Loads the data for the character"""
    data = get_xombp_data()
    char = data.get_character_by_id(char_id)
    if char:
        return char
    with open(Path(f"data/characters/{char_id}.json"), encoding='utf-8') as data_file:
        return json.load(data_file)


def get_character_party(character: dict[str, Any]):
    if "parties" not in character or len(character['parties']) == 0:
        return None
    return sorted(character['parties'].values(),
           key=lambda p: dateutil.parser.parse(p['start']),
           reverse=True)[0]['party']

def find_character(name_or_id: str) -> list[dict]:
    """Specifically to be used by the CLI to find characters based on name or id"""
    if name_or_id.isnumeric():
        return [load_character(int(name_or_id))]

    characters = get_all_characters()
    search = re.compile(re.escape(name_or_id), re.IGNORECASE)
    found = []
    for char_id, char in characters.items():
        if search.search(char['name']):
            found.append(load_character(char_id))
    return found


def get_all_characters() -> dict[str, dict]:
    """Returns a list of all of the characters"""
    if not ALL_CHARACTERS_PATH.exists():
        update_character_list()
        print("Updating character list")
    with open(ALL_CHARACTERS_PATH, encoding='utf-8') as data_file:
        return json.load(data_file)


def update_character_list() -> None:
    """Updates the character list cache"""
    characters_path = Path("data/characters")
    characters = {}
    for char_path in characters_path.glob("*.json"):
        with open(char_path, encoding='utf-8') as infile:
            char = json.load(infile)
            characters[char['id']] = {
                'name': char['name']
            }

    ALL_CHARACTERS_PATH.parent.mkdir(exist_ok=True, parents=True)
    with open(ALL_CHARACTERS_PATH, "w", encoding='utf-8') as outfile:
        json.dump(characters, outfile, indent=2)
