"""
This file contains functions and classes related to
political parties
"""

from pathlib import Path
import json


__author__ = "Preben Vangberg"
__copyright__ = "Copyright 2023, XO's Model British Parliament"
__credits__ = ["Preben Vangberg"]
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Preben Vangberg"
__email__ = "prvinspace@gmail.com"
__status__ = "Production"


def load_party(party_id: str) -> dict:
    """Loads the data for the party"""
    with open(Path(f"data/parties/{party_id}.json"), encoding='utf-8') as data_file:
        return json.load(data_file)
