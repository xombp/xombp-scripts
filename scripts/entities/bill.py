"""
This file contains functions and classes related to
bills and general legislation
"""

from dataclasses import dataclass
from datetime import date
import json
from pathlib import Path
import re
from typing import Any
import datetime
import markdown

from entities.character import load_character
from services.spreadsheet import get_xombp_data


__author__ = "Preben Vangberg"
__copyright__ = "Copyright 2023, XO's Model British Parliament"
__credits__ = ["Preben Vangberg"]
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Preben Vangberg"
__email__ = "prvinspace@gmail.com"
__status__ = "Production"


BILL_INTRODUCTION = re.compile(
    ".*Be it enacted by the Queen's most Excellent Majesty.*", re.IGNORECASE)

ENACTING_FORMULAS = {
    "generic": """
<span class="smallcaps">Be it enacted</span> by the King's most Excellent Majesty, by and with the advice and consent of
the Lords Spiritual and Temporal, and Commons, in this present Parliament assembled, and by
the authority of the same, as follows
""",
    "finance": """
We Your Majesty's most dutiful and loyal subjects the Commons of the United Kingdom in Parliament
assembled, towards raising the necessary supplies to defray Your Majesty's public expenses and making
an addition to the public revenue have freely and voluntarily resolved to give and grant unto
Your Majesty the several duties hereinafter mentioned and do therefore most humbly beseech Your Majesty
that it may be it enacted and be it enacted by the King's Most Excellent Majesty, by and with the
advice and consent of the Lords Spiritual and Temporal, and Commons, in this present Parliament
assembled, and by the authority of the same as follows
"""
}


@dataclass
class Bill:
    """Represents a single bill"""
    data: dict
    term: int

    def save(self):
        bill_path = get_bill_path(self.data['id'], self.term)
        with open(bill_path / "bill.json", "w") as data_file:
            return json.dump(self.data, data_file, indent=2)


    def get_version_md(self, version):
        """Returns the Markdown content of the version"""
        bill_path = get_bill_path(self.data['id'], self.term)
        with open(bill_path / 'versions' / f'{version}.md', encoding='utf-8') as version_file:
            return version_file.read()

    def get_version_html(self, version):
        """Converts the version to HTML"""
        return markdown.markdown(self.get_version_md(version))


@dataclass
class BillComponents:
    """Represents the components found in a Markdown file"""

    id: str
    title: str
    long_title: str
    content: str

    def export_as_json(self) -> dict[str, Any]:
        """Generates the bare-bones default json file for the bill"""
        return {
            "id": self.id,
            "title": self.title,
            "long_title": self.long_title,
            "effective_title": self.title + " Bill",
            "effective_long_title": "A bill to " + self.long_title,
            "type": "bill",
            "authors": [],
            "submission_type": "private",
            "stages": {
                "0": {
                    "date": str(date.today()),
                    "type": "first",
                    "house": "commons" if self.id.startswith("C") else "lords"
                }
            },
            "versions": {
                "0": {
                    "date": str(date.today()),
                    "id": "submitted",
                    "title": "As submitted"
                }
            }
        }


def get_components_from_md(path, bill_id) -> BillComponents:
    """Reads the title, long title, and content from a Markdown file"""
    with open(path, encoding='utf-8') as file:
        text = file.readlines()
    title = text.pop(0).replace("# ", "")
    title = re.sub(r"bill( 20[0-9]{2})?", "",
                   title, flags=re.IGNORECASE).strip()

    while not text[0] or text[0].isspace():
        text.pop(0)
    long_title = text.pop(0).replace("*", "")
    long_title = re.sub("^an? (act|bill) to", "",
                        long_title, flags=re.IGNORECASE).strip()
    text = filter(
        lambda x: not BILL_INTRODUCTION.match(x),
        text
    )
    return BillComponents(
        bill_id.upper(),
        title,
        long_title,
        ''.join(text).strip() + '\n'
    )

def check_if_bill_is_valid(bill_id) -> bool:
    """Checks if the data file for the bill exists"""
    data_path = get_bill_path(bill_id) / "bill.json"
    return data_path.exists()

def get_bill_path(bill_id: str, bill_term: int = None) -> Path:
    """Returns the expected path of a bill based on the ID of the bill"""
    bill_id = bill_id.lower()
    if not bill_term:
        bill_term = int(bill_id.split('-')[0][2:])
        assert 0 < bill_term < 15
    return Path(f"data/bills/term_{bill_term}/{bill_id}")


def load_bill(bill_id: str, term: int = None, raw: bool = False) -> Bill:
    """Loads the data from the bill and fills it with the required content"""
    if not term:
        term = int(bill_id.split('-')[0][2:])
    bill_path = get_bill_path(bill_id, term)
    with open(bill_path / 'bill.json', encoding="utf-8") as data_file:
        data = json.load(data_file)
    if 'versions' not in data:
        data['versions'] = {}
    if raw:
        return Bill(
            data,
            term
        )
    
    stages_from_spreadsheet = fetch_readings_for_bill(bill_id.upper())
    if stages_from_spreadsheet:
        data['stages'] = stages_from_spreadsheet 
    if 'enacted' in data:
        next_index = max(map(int, data['stages'])) + 1
        data['stages'][str(next_index)] = {
            "date": data['enacted'],
            "type": "royal_assent",
            "house": "royals"
        }

    # Fix the authors
    characters = {}
    add_names_to_author_dictionary(data['authors'], characters)

    if 'amendments' in data:
        for amendment_id, amendment in data['amendments'].items():
            if 'authors' in amendment:
                add_names_to_author_dictionary(amendment['authors'], characters)
            amdt_path = bill_path / "amendments" / f"{amendment_id}.md"
            amendment['text'] = markdown.markdown(amdt_path.read_text())
            print(amendment)

    # Fix divisions
    for stage in data['stages'].values():
        if not 'divisions' in stage:
            continue
        for division in stage['divisions'].values():
            for vote_type in ['in_favour', 'abstentions', 'against', 'did_not_vote']:
                if vote_type not in division:
                    continue
                for vote in division[vote_type]:
                    if vote not in characters:
                        characters[vote] = load_character(vote)
                    division[vote_type][vote]['name'] = characters[vote]['name']

    # Add enacting formula
    enacting_formula_path = bill_path / "enacting_formula.md"
    if enacting_formula_path.exists():
        data["enacting_formula"] = markdown.markdown(enacting_formula_path.read_text().strip())
    else:
        data["enacting_formula"] = ENACTING_FORMULAS["finance" if data['title']
                                                 == 'Finance' else 'generic'].strip().replace('\n', ' ')
    if 'introduced' not in data:
        data['introduced'] = data['stages']["0"]['date']

    # Add the effective name and longtitle
    if 'enacted' in data:
        enacted = datetime.datetime.strptime(data['enacted'], '%Y-%m-%d')
        data['effective_title'] = f"{data['title']} Act {enacted.year}"
        data['effective_long_title'] = f"An Act to {data['long_title']}"

    return Bill(
        data,
        term
    )


def fetch_readings_for_bill(bill_id: str) -> dict:
    data = get_xombp_data()
    bills = data.get_bills()
    bill_rows = bills[bills['Bill No.'].str.startswith(bill_id.upper())]
    if len(bill_rows) == 0:
        return None
    houses = ["commons", "lords" ] if bill_rows.iloc[0]["House 1"] == "C" else ["lords", "commons"]
    readings = {}
    reading_index = 0
    print(houses)
    for _, row in bill_rows.iterrows():
        for column in bills.columns[bills.columns.str.contains("^House [1-9] ", regex=True)]:
            if row[column]:
                house = houses[int(column.split()[1]) - 1 % 2]
                reading_type = get_reading_type(column)
                print(column, row[column], house, reading_type)
                readings[str(reading_index)] = {
                    "house": house,
                    "type": reading_type,
                    "date": row[column]
                }
                reading_index += 1

    return readings


def get_reading_type(column) -> str:
    if "1st" in column:
        return "first"
    if "2nd" in column:
        return "second"
    if "Report" in column:
        return "report"
    if "3rd" in column:
        return "third"
    if "Consideration" in column:
        return "consideration"
    raise ValueError(f"Unknown reading type {column!r}")

def add_names_to_author_dictionary(dictionary: dict[str], characters) -> None:
    for character_id, character_data in dictionary.items():
        if character_id not in characters:
            xombp_data = get_xombp_data()
            characters[character_id] = xombp_data.get_character_by_id(int(character_id))
        name = characters[character_id]['Name']
        character_data['name'] = name