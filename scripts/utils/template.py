"""
This file contains helper functions to make it easier
to use jinja2 templates
"""


from jinja2 import Environment, FileSystemLoader, Template


__author__ = "Preben Vangberg"
__copyright__ = "Copyright 2023, XO's Model British Parliament"
__credits__ = ["Preben Vangberg"]
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Preben Vangberg"
__email__ = "prvinspace@gmail.com"
__status__ = "Production"


def get_jinja2_template(name: str) -> Template:
    """Loads a template from the template folder with the given name"""
    template_env = Environment(loader=FileSystemLoader('templates'))
    return template_env.get_template(name)
