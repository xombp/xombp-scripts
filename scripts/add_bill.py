#!/usr/bin/env python3
"""
This script is responsible for generating the default files for
a bill based on the bill.md file located in the bill's directory
"""

import argparse
from pathlib import Path
import json

from services import discord
import services.spreadsheet as spreadsheet
from entities.bill import get_components_from_md
from entities.character import load_character
import services.webhooks as webhooks
from utils.template import get_jinja2_template
from publish_bill import publish_bill, publish_first_reading
from entities.house import House


__author__ = "Preben Vangberg"
__copyright__ = "Copyright 2023, XO's Model British Parliament"
__credits__ = ["Preben Vangberg"]
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Preben Vangberg"
__email__ = "prvinspace@gmail.com"
__status__ = "Production"


def main():
    """The main function of the script"""
    args = parse_args()

    bill_id = args.bill_id.lower()
    if args.term:
        bill_term = args.term
    else:
        bill_term = int(bill_id.split('-')[0][2:])
    if not bill_term:
        raise RuntimeError("No bill term provided")

    bill_path = Path(f"data/bills/term_{bill_term}/{bill_id}")

    print(f"Term {bill_term}: {bill_id.upper()}")
    print(f"Path: {bill_path}")
    bill = get_components_from_md(bill_path / 'bill.md', bill_id)
    print(f"Title: {bill.title} Bill")
    print(f"Long title: A bill to {bill.long_title}")
    authors = resolve_authors(args.authors)

    if args.dry:
        return

    json_data = bill.export_as_json()
    json_data['submission_type'] = args.type
    json_data['authors'] = {
        author: {"party":  load_character(author)['Party ID']}
        for author in authors
    }
    
    data_path = bill_path / "bill.json"

    if not args.force and data_path.exists():
        raise RuntimeError(
            "Data file for bill already exists, are you sure you want to override it? In that case, use the --force flag")

    with open(data_path, "w", encoding="utf-8") as bill_info:
        json.dump(json_data, bill_info, indent=2)
    submitted_path = bill_path / "versions" / "submitted.md"
    submitted_path.parent.mkdir(exist_ok=True, parents=True)
    with open(submitted_path, "w", encoding="utf-8") as content:
        content.write(bill.content)

    if args.publish:
        publish_bill(bill_id)
        #publish_first_reading(bill_id, bill_term)



def resolve_authors(authors: list[str]) -> set[int]:
    """Resolves the list of authors provided through the CLI"""
    resolved_authors = []
    for author in authors:
        XOMBP_DATA = spreadsheet.get_xombp_data()
        found_authors = XOMBP_DATA.get_character_by_name(author)
        if len(found_authors) < 1:
            raise RuntimeError(f"No author found for search {author!r}")
        if len(found_authors) > 1:
            raise RuntimeError(
                f"Several authors found for search {author!r} ({', '.join(map(lambda x: x['name'], found_authors))})")
        resolved_authors.append(found_authors[0])
    print(
        f"Authors: {', '.join(sorted(map(lambda c: c['Name'], resolved_authors)))}")
    return set(map(lambda c: c['ID'], resolved_authors))


def parse_args() -> argparse.Namespace:
    """Parses the arguments from the command line"""
    parser = argparse.ArgumentParser(
        "Publishes a bill to the required places"
    )
    parser.add_argument("bill_id")
    parser.add_argument("--term", nargs="?")
    parser.add_argument("--authors", nargs="+", required=True,
                        help="The authors of the bill")
    parser.add_argument("--dry", action="store_true",
                        help="Resolves all of the components, but does not write to disk")
    parser.add_argument("--force", action="store_true",
                        help="Forcefully overwrite existing data")
    parser.add_argument("--publish", action="store_true",
                        help="Publishes the bill as well")
    parser.add_argument("--type", default="private", choices=["private", "government", "opposition"])
    return parser.parse_args()


if __name__ == "__main__":
    main()
